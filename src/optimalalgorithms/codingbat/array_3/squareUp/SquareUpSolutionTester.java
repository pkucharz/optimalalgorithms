package optimalalgorithms.codingbat.array_3.squareUp;

import java.util.Arrays;
import java.util.Collection;
import optimalalgorithms.SolutionTester;

public class SquareUpSolutionTester extends SolutionTester<SquareUpSolver> {

	public static void main(String[] args) {
		new SquareUpSolutionTester().test();
	}

	@Override
	protected Collection<SquareUpSolver> solvers() {
		return Arrays.asList(
				new MatrixSolver(),
				new OptimizedMatrixSolver(),
				new HalfMatrixSolver());
	}

	@Override
	protected void randomSolverEvaluation(SquareUpSolver solver) {
		solver.squareUp(100);
	}

	protected int getNumberOfRandomSolverEvaluations() {
		return 100000;
	}

	@Override
	protected void checkSolver(SquareUpSolver solver) {
		failIfFalse(Arrays.equals(solver.squareUp(2), new int[]{0, 1, 2, 1}));
		failIfFalse(Arrays.equals(solver.squareUp(3), new int[]{0, 0, 1, 0, 2, 1, 3, 2, 1}));
		failIfFalse(Arrays.equals(solver.squareUp(4), new int[]{0, 0, 0, 1, 0, 0, 2, 1, 0, 3, 2, 1, 4, 3, 2, 1}));
		failIfFalse(Arrays.equals(solver.squareUp(5),
				new int[]{0, 0, 0, 0, 1, 0, 0, 0, 2, 1, 0, 0, 3, 2, 1, 0, 4, 3, 2, 1, 5, 4, 3, 2, 1}));
	}
}
