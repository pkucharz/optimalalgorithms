package optimalalgorithms.codingbat.array_3.squareUp;

public class HalfMatrixSolver implements SquareUpSolver {

	@Override
	public int[] squareUp(int n) {
		int[] squareUp = new int[n * n];
		for (int row = 0; row < n; row++) {
			for (int col = n - row - 1; col < n; col++) {
				int idx = row * n + col;
				squareUp[idx] = n - idx % n;
			}
		}
		return squareUp;
	}
	
}