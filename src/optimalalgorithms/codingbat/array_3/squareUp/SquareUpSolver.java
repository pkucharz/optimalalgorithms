package optimalalgorithms.codingbat.array_3.squareUp;

import optimalalgorithms.Solver;

interface SquareUpSolver extends Solver{
	int[] squareUp(int n);
}
