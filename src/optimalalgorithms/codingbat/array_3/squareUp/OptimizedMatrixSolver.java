package optimalalgorithms.codingbat.array_3.squareUp;

public class OptimizedMatrixSolver implements SquareUpSolver {

	@Override
	public int[] squareUp(int n) {
		int[] squareUp = new int[n * n];
		for (int row = 0; row < n; row++) {
			for (int col = 0; col < n; col++) {
				// Do not perform set operation as array of ints is by default initialized to zeros
				if (col + 1 >= n - row) {
					int idx = row * n + col;
					squareUp[idx] = n - idx % n;
				}
			}
		}
		return squareUp;
	}
}