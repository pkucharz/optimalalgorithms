package optimalalgorithms.codingbat.array_3.squareUp;

public class MatrixSolver implements SquareUpSolver {

	// n=2
	// 0 1
	// 2 1

	// n=4
	// 0 0 0 1
	// 0 0 2 1
	// 0 3 2 1
	// 4 3 2 1

	// n=5
	// 0 0 0 0 1
	// 0 0 0 2 1
	// 0 0 3 2 1
	// 0 4 3 2 1
	// 5 4 3 2 1

	@Override
	public int[] squareUp(int n) {
		int[] squareUp = new int[n * n];
		for (int row = 0; row < n; row++) {
			for (int col = 0; col < n; col++) {
				int idx = row * n + col;
				int value = n - idx % n;
				squareUp[idx] = col + 1 < n - row ? 0 : value;
			}
		}
		return squareUp;
	}
}