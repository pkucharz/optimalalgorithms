package optimalalgorithms;

import java.util.Collection;
import java.util.Random;
import java.util.TreeSet;

public abstract class SolutionTester<T extends Solver> {

	private TreeSet<SolverBenchmark> benchmarks = new TreeSet<>();

	protected Random random = new Random();

	protected abstract Collection<T> solvers();
	protected abstract void randomSolverEvaluation(T solver);
	protected abstract void checkSolver(T solver);

	protected void test() {
		checkAllSolvers();
		betchmarkAllSolvers();
	}

	private void betchmarkAllSolvers() {
		for (T solver : solvers()) {
			benchmarks.add(betchmark(solver));
		}
		printBenchmarks();
	}

	private void checkAllSolvers() {
		System.out.println("Checking solvers:");
		System.out.println("========================================================");
		for (T solver : solvers()) {
			String result = "OK";
			try {
				checkSolver(solver);
			} catch (Exception e) {
				result = "ERROR [" + e.getStackTrace()[1].toString() + "]";
			}
			System.out.printf("%1$-50s %2$-100s\n", solver.getClass().getSimpleName(), result);
		}
		System.out.println();
	}

	private void printBenchmarks() {
		System.out.println("Benchmarks:");
		System.out.println("========================================================");
		for (SolverBenchmark benchmark : benchmarks) {
			System.out.println(benchmark);
		}
		System.out.println();
	}

	protected void failIfFalse(boolean condition) {
		if (!condition) {
			throw new InvalidSolverResultRuntimeException();
		}
	}

	protected int getNumberOfRandomSolverEvaluations() {
		return 1000000;
	}

	private SolverBenchmark betchmark(T solver) {
		long start = System.currentTimeMillis();
		for (int i = 0; i < getNumberOfRandomSolverEvaluations(); i++) {
			randomSolverEvaluation(solver);
		}
		return new SolverBenchmark(solver, System.currentTimeMillis() - start);
	}

	class SolverBenchmark implements Comparable<SolverBenchmark> {
		private Solver solver;
		private long executionTimeInMillis;

		SolverBenchmark(Solver solver, long executionTimeInMillis) {
			this.solver = solver;
			this.executionTimeInMillis = executionTimeInMillis;
		}

		@Override
		public int compareTo(SolverBenchmark o) {
			int timeDiff = (int) (executionTimeInMillis - o.executionTimeInMillis);
			return timeDiff != 0
					? timeDiff
					: getClass().getSimpleName().compareTo(o.solver.getClass().getSimpleName());
		}

		@Override
		public String toString() {
			return String.format("%1$-40s %2$10d ms", solver.getClass().getSimpleName(), executionTimeInMillis);
		}
	}

	public static class InvalidSolverResultRuntimeException extends RuntimeException {
	}
}
