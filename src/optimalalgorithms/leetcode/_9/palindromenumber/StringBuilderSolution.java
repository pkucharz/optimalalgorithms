package optimalalgorithms.leetcode._9.palindromenumber;

public class StringBuilderSolution {

	public boolean isPalindrome(int x) {
		String original = String.valueOf(x);
		String reversed = new StringBuilder(original).reverse().toString();
		return original.equals(reversed);
	}
}
