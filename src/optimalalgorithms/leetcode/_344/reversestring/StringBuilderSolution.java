package optimalalgorithms.leetcode._344.reversestring;

public class StringBuilderSolution {

	public String reverseString(String s) {
		return new StringBuilder(s).reverse().toString();
	}
}
