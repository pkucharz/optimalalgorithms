package optimalalgorithms.leetcode._520.detectcapital;

public class OrSolution {

	// 42.31 %
	public boolean detectCapitalUse(String word) {
		boolean isAllUppercased = word.toUpperCase().equals(word);
		boolean isAllLowercased = word.toLowerCase().equals(word);
		boolean isFirstLetterCapitalizedOnly = Character.isUpperCase(word.charAt(0)) && word.length() > 1
				&& !upperCaseDetected(word.substring(1));
		return isAllUppercased || isAllLowercased || isFirstLetterCapitalizedOnly;
	}

	private boolean upperCaseDetected(String word) {
		for (char c : word.toCharArray()) {
			if (Character.isUpperCase(c)) {
				return true;
			}
		}
		return false;
	}
}
