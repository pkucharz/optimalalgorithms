package optimalalgorithms.leetcode._191.numberof1bits;

public class CharArraySolution {

	// 2.61 %
	public int hammingWeight(int n) {
		int hammingWeight = 0;
		char[] bits = Integer.toBinaryString(n).toCharArray();
		for (char bit : bits) {
			if (bit == '1') {
				hammingWeight++;
			}
		}
		return hammingWeight;
	}
}
