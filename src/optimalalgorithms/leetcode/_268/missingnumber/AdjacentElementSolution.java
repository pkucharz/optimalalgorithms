package optimalalgorithms.leetcode._268.missingnumber;

import java.util.Arrays;

public class AdjacentElementSolution {

	// 8.82 %
	public int missingNumber(int[] nums) {
		Arrays.sort(nums);
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] != i) {
				return i;
			}
		}
		return nums.length;
	}
}
