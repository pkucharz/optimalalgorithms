package optimalalgorithms.leetcode._387.firstuniquecharacterinastring;

public class IndexOfSolution {

	// 36.01%
	public int firstUniqChar(String s) {
		if (s.isEmpty()) {
			return -1;
		}
		char[] chars = s.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (isUnique(s, chars[i])) {
				return i;
			}
		}
		return -1;
	}

	private boolean isUnique(String s, char c) {
		return s.indexOf(c) == s.lastIndexOf(c);
	}
}
