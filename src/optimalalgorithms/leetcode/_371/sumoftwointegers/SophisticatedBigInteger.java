package optimalalgorithms.leetcode._371.sumoftwointegers;

public class SophisticatedBigInteger {

	// 1.56 %
	public int getSum(int a, int b) {
		return java.math.BigInteger.valueOf(a).add(java.math.BigInteger.valueOf(b)).intValue();
	}
}
