package optimalalgorithms.leetcode._169.majorityelement;

import java.util.Arrays;

public class AdjacentDuplicatesSolution {

	// 32.47 %
	public int majorityElement(int[] nums) {
		if (nums.length == 1) {
			return nums[0];
		}
		int threshold = nums.length / 2;
		Arrays.sort(nums);
		for (int i = 0; i < nums.length;) {
			int nextIndex = i + 1;
			int duplicateCount = 1;
			while ((nextIndex < nums.length) && (nums[nextIndex++] == nums[i])) {
				if (++duplicateCount > threshold) {
					return nums[i];
				}

			}
			i += duplicateCount;
		}
		throw new java.util.NoSuchElementException();
	}

	public static void main(String[] args) {
		AdjacentDuplicatesSolution fc = new AdjacentDuplicatesSolution();
		int[] nums = { 3, 2, 3 };
		System.out.println(fc.majorityElement(nums));
	}
}
