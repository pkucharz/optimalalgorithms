package optimalalgorithms.leetcode._7.reverseinteger;

import java.math.BigInteger;

public class BigIntegerSolution {

	public int reverse(int x) {
		String unsigned = String.valueOf(Math.abs(x));
		if (Integer.MIN_VALUE == x) {
			unsigned = unsigned.substring(1);
		}
		String reversedUnsigned = new StringBuilder(unsigned).reverse().toString();
		BigInteger bigInteger = new BigInteger(reversedUnsigned);
		if (bigInteger.compareTo(new BigInteger(String.valueOf(Integer.MAX_VALUE))) > 0) {
			return 0;
		}
		return bigInteger.intValue() * (x < 0 ? -1 : 1);
	}
}
