package optimalalgorithms.leetcode._136.singlenumber;

import java.util.Arrays;

public class ExclusiveOrSolution {

    //36.96%
    public int singleNumber(int[] nums) {
        if(nums.length == 1) return nums[0];

        for (int i = 1; i < nums.length; i++) {
            nums[0] = nums[0] ^ nums[i];
        }

        return nums[0];
    }

    //26.82%
    public int singleNumberRecursive(int[] nums) {
        return nums.length == 1 ? nums[0] : xorNumber(nums, 0, nums.length/2 - 1) ^ xorNumber(nums, nums.length/2, nums.length-1);
    }

    private int xorNumber(int[] nums, int start, int finish){
        if(start == finish) {
            return nums[start];
        } else {
            return nums[start] ^ xorNumber(nums, start + 1, finish);
        }
    }

    //2.67%
    public int singleNumberAsStream(int[] nums) {
        return Arrays.stream(nums).reduce(0, (a,b) -> a ^ b);
    }

    //2.09%
    public int singleNumberAsParallelStream(int[] nums) {
        return Arrays.stream(nums).parallel().reduce(0, (a,b) -> a ^ b);
    }

}
