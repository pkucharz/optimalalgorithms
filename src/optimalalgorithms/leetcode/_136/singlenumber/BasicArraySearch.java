package optimalalgorithms.leetcode._136.singlenumber;

public class BasicArraySearch {

    //2.14%
	public int singleNumber(int[] nums) {
		if (nums.length == 1) {
			return nums[0];
		}
		for (int i = 0; i < nums.length; i++) {
			int current = nums[i];
			if (hasNoDuplicates(nums, current, i)) {
				return current;
			}
		}
		return 0;
	}

	private boolean hasNoDuplicates(int[] nums, int element, int originalIndex) {
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == element && originalIndex != i) {
				return false;
			}
		}
		return true;
	}
}
