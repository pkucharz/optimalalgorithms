package optimalalgorithms.leetcode._136.singlenumber;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapBased {

	public int singleNumber(int[] nums) {
		if (nums.length == 1)
			return nums[0];
		Map<Integer, Boolean> map = new HashMap<>(nums.length);
		for (int i = 0; i < nums.length; i++) {
			int element = nums[i];
			Boolean duplicate = map.get(element);
			if (duplicate != null && duplicate) {
				map.put(element, Boolean.TRUE);
			} else {
				map.put(element, Boolean.FALSE);
			}
		}
		for (Entry<Integer, Boolean> entry : map.entrySet()) {
			if (!entry.getValue()) {
				return entry.getKey();
			}
		}
		return 0;
	}
}
