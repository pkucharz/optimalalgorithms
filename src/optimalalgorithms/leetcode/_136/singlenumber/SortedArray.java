package optimalalgorithms.leetcode._136.singlenumber;

import java.util.Arrays;

public class SortedArray {

	public int singleNumber(int[] nums) {
		if (nums.length == 1) {
			return nums[0];
		}
		Arrays.sort(nums);
		for (int i = 0; i < nums.length; i++) {
			int current = nums[i];
			// single element is the first one
			boolean leftExists = i - 1 >= 0;
			boolean rightExists = i + 1 < nums.length;
			if (!leftExists && rightExists && current != nums[i + 1]) {
				return current;
			}
			// single element is in the middle
			if (leftExists && current != nums[i - 1] && rightExists && current != nums[i + 1]) {
				return current;
			}
			// single element is the last one
			if (!rightExists && leftExists && current != nums[i - 1]) {
				return current;
			}
		}
		return 0;
	}
}
