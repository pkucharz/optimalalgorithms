package optimalalgorithms.leetcode._136.singlenumber;

import java.util.HashSet;
import java.util.Set;

public class SetBased {

	public int singleNumber(int[] nums) {
		if (nums.length == 1) {
			return nums[0];
		}
		Set<Integer> set = new HashSet<>(nums.length / 2 + 1);
		for (int i = 0; i < nums.length; i++) {
			Integer current = Integer.valueOf(nums[i]);
			if (set.contains(current)) {
				set.remove(current);
			} else {
				set.add(current);
			}
		}
		return set.iterator().next();
	}
}
