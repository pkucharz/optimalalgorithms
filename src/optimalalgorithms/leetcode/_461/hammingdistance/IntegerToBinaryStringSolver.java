package optimalalgorithms.leetcode._461.hammingdistance;

public class IntegerToBinaryStringSolver implements HummingDistanceSolver {

	// 2.08%
	public int hammingDistance(int x, int y) {
		int hammingDistance = 0;
		final char one = '1';
		String binaryString = Integer.toBinaryString(x ^ y);
		for (char c : binaryString.toCharArray()) {
			if (c == one) {
				hammingDistance++;
			}
		}
		return hammingDistance;
	}
}
