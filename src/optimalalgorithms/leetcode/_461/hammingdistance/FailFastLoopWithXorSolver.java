package optimalalgorithms.leetcode._461.hammingdistance;

class FailFastLoopWithXorSolver implements HummingDistanceSolver {

	@Override
	public int hammingDistance(int x, int y) {
		int xyXOR = x ^ y;
		int hd = 0;
		for (int i = 0; i < 31; i++) {
			int next = xyXOR >> i;
			if (next == 0) {
				return hd;
			}
			hd += next & 0x01;
		}
		return hd;
	}
}
