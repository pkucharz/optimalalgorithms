package optimalalgorithms.leetcode._461.hammingdistance;

class LoopWithXorSolver implements HummingDistanceSolver {

	@Override
	public int hammingDistance(int x, int y) {
		int xyXOR = x ^ y;
		int hd = 0;
		for (int i = 0; i < 31; i++) {
			hd += (xyXOR >> i) & 0x01;
		}
		return hd;
	}
}
