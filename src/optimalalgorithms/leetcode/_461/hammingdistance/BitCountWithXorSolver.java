package optimalalgorithms.leetcode._461.hammingdistance;

class BitCountWithXorSolver implements HummingDistanceSolver {

	@Override
	public int hammingDistance(int x, int y) {
		return Integer.bitCount(x ^ y);
	}

}
