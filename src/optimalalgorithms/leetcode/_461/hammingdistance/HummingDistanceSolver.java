package optimalalgorithms.leetcode._461.hammingdistance;

import optimalalgorithms.Solver;

interface HummingDistanceSolver extends Solver {
	int hammingDistance(int x, int y);
}
