package optimalalgorithms.leetcode._461.hammingdistance;

import java.util.Arrays;
import java.util.Collection;
import optimalalgorithms.SolutionTester;

public class HummingDistanceSolutionTester extends SolutionTester<HummingDistanceSolver> {

	public static void main(String[] args) {
		new HummingDistanceSolutionTester().test();
	}

	@Override
	protected Collection<HummingDistanceSolver> solvers() {
		return Arrays.asList(
				new LoopWithXorSolver(),
				new FailFastLoopWithXorSolver(),
				new BitCountWithXorSolver(),
				new IntegerToBinaryStringSolver());                      	
	}

	@Override
	protected void randomSolverEvaluation(HummingDistanceSolver solver) {
		solver.hammingDistance(random.nextInt(), random.nextInt());
	}

	@Override
	protected void checkSolver(HummingDistanceSolver solver) {
		checkHD(solver, 0, 0, 0);
		checkHD(solver, 0, 1, 1);
		checkHD(solver, 1, 1, 0);
		checkHD(solver, 0, 2, 1);
		checkHD(solver, 1, 2, 2);
		checkHD(solver, 2, 1, 2);
		checkHD(solver, 0, 7, 3);
		checkHD(solver, 7, 0, 3);
		checkHD(solver, 93, 73, 2);
		checkHD(solver, Integer.MAX_VALUE, 0, 31);
	}

	private void checkHD(HummingDistanceSolver solver, int x, int y, int expectedHD) {
		failIfFalse(solver.hammingDistance(x, y) == expectedHD);
	}

}
