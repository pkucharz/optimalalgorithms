package optimalalgorithms.leetcode._217.containsduplicates;

import java.util.Arrays;

public class BinarySearchSolution {

	// 86.50 %
	public boolean containsDuplicate(int[] nums) {
		if (nums.length <= 1) {
			return false;
		}
		Arrays.sort(nums);
		for (int i = 0; i < (nums.length - 1); i++) {
			if (Arrays.binarySearch(nums, i + 1, nums.length, nums[i]) >= 0) {
				return true;
			}
		}
		return false;
	}
}
