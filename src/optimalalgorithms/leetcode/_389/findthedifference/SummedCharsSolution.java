package optimalalgorithms.leetcode._389.findthedifference;

public class SummedCharsSolution {

	// 77.59 %
	public char findTheDifference(String s, String t) {
		char[] sArray = s.toCharArray();
		char[] tArray = t.toCharArray();
		int tSummary = 0;
		for (char tChar : tArray) {
			tSummary += tChar;
		}
		for (char sChar : sArray) {
			tSummary -= sChar;
		}
		return (char) tSummary;
	}
}
