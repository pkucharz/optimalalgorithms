package optimalalgorithms.leetcode._242.validanagram;

public class CharArrayBasedSolution {

	// 0.65%
	public boolean isAnagram(String s, String t) {
		return s.length() == t.length() && containsAll(s.toCharArray(), t.toCharArray());
	}

	private boolean containsAll(char[] s, char[] t) {
		for (char c : t) {
			int indexOf = charIndex(s, c);
			if (indexOf == -1) {
				return false;
			}
			nullify(s, indexOf);
		}
		return true;
	}

	private int charIndex(char[] s, char c) {
		for (int i = 0; i < s.length; i++) {
			if (s[i] == c) {
				return i;
			}
		}
		return -1;
	}

	private void nullify(char[] s, int indexOf) {
		s[indexOf] = '\u0000';
	}
}
