package optimalalgorithms.leetcode._242.validanagram;

public class IndexOfSolution {

	// 0.86%
	public boolean isAnagram(String s, String t) {
		return s.length() == t.length() && containsAll(new StringBuilder(s), t);
	}

	private boolean containsAll(StringBuilder s, String t) {
		for (char c : t.toCharArray()) {
			int indexOf = s.indexOf(String.valueOf(c));
			if (indexOf == -1) {
				return false;
			}
			s.deleteCharAt(indexOf);
		}
		return true;
	}
}
