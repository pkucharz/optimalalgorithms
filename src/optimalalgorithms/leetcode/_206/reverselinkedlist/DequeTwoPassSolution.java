package optimalalgorithms.leetcode._206.reverselinkedlist;

import java.util.ArrayDeque;
import java.util.Deque;

public class DequeTwoPassSolution {

	// 2.25 %
	public ListNode reverseList(ListNode head) {
		if (head == null) {
			return null;
		}
		if (head.next == null) {
			return head;
		}
		Deque<ListNode> nodes = new ArrayDeque<>();
		ListNode current = head;
		while (current != null) {
			nodes.push(current);
			current = current.next;
		}
		ListNode tail = nodes.removeFirst();
		nodes.getLast().next = null;
		current = tail;
		while (!nodes.isEmpty()) {
			current.next = nodes.pop();
			current = current.next;
		}
		return tail;
	}

}
