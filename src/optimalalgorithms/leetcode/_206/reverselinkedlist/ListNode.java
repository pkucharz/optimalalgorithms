package optimalalgorithms.leetcode._206.reverselinkedlist;

public class ListNode {

	int val;

	ListNode next;

	ListNode(int x) {
		val = x;
	}

	@Override
	public String toString() {
		return String.format("{%d}", val);
		// ListNode node = this;
		// StringBuilder sb = new StringBuilder();
		// sb.append(node.val).append(' ');
		// while (node.next != null) {
		// node = node.next;
		// sb.append(node.val).append(' ');
		// }
		// return sb.toString();
	}
}
