package optimalalgorithms.leetcode._206.reverselinkedlist;

public class ListNodePrinter {
	private ListNodePrinter() {}

	public static void print(ListNode head) {
		ListNode node = head;
		System.out.print(node.val + " ");
		while (node.next != null) {
			node = node.next;
			System.out.print(node.val + " ");
		}
		System.out.println();
	}
}
