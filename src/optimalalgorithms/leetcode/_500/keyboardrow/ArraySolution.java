package optimalalgorithms.leetcode._500.keyboardrow;

public class ArraySolution {

	// 63.43%
	public String[] findWords(String[] words) {
		java.util.Collection<String> matchingWords = new java.util.ArrayList<>(words.length);
		char[] qwerty = { 'e', 'i', 'o', 'p', 'q', 'r', 't', 'u', 'w', 'y' };
		char[] asdfgh = { 'a', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 's' };
		char[] zxcvb = { 'b', 'c', 'm', 'n', 'v', 'x', 'z' };
		for (String word : words) {
			boolean wordMatches = true;
			char firstLetter = Character.toLowerCase(word.charAt(0));
			char[] rowToMatch = (java.util.Arrays.binarySearch(qwerty, firstLetter) >= 0) ? qwerty
					: (java.util.Arrays.binarySearch(asdfgh, firstLetter) >= 0) ? asdfgh : zxcvb;
			char[] lowerCasedChars = word.toLowerCase().toCharArray();
			for (char ch : lowerCasedChars) {
				wordMatches &= java.util.Arrays.binarySearch(rowToMatch, ch) >= 0;
				if (!wordMatches) {
					break;
				}
			}
			if (wordMatches) {
				matchingWords.add(word);
			}

		}
		return matchingWords.toArray(new String[] {});
	}

	public static void main(String[] args) {
		char[] qwerty = { 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p' };
		char[] asdfgh = { 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l' };
		char[] zxcvb = { 'z', 'x', 'c', 'v', 'b', 'n', 'm' };
		java.util.Arrays.sort(qwerty);
		java.util.Arrays.sort(asdfgh);
		java.util.Arrays.sort(zxcvb);
		print(qwerty);
		print(asdfgh);
		print(zxcvb);
	}

	private static void print(char[] chars) {
		System.out.print('{');
		for (char c : chars) {
			System.out.printf("'%c',", c);
		}
		System.out.print('}');
	}
}
