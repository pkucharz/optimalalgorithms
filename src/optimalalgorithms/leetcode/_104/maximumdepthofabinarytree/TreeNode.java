package optimalalgorithms.leetcode._104.maximumdepthofabinarytree;

public class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}