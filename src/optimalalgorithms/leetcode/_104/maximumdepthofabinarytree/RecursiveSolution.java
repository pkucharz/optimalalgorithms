package optimalalgorithms.leetcode._104.maximumdepthofabinarytree;

import java.util.Random;

public class RecursiveSolution {

	// 18.27 %
	public int maxDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return Math.max(1 + maxDepth(root.left), 1 + maxDepth(root.right));
	}

	public static void main(String[] args) {
		RecursiveSolution rs = new RecursiveSolution();
		TreeNode root = node();
		root.left = node();
		root.right = node();
		root.right.left = node();
		root.right.right = node();
		root.right.right.right = node();
		System.out.println(rs.maxDepth(root));
	}

	static TreeNode node() {
		return new TreeNode(new Random().nextInt(100));
	}
}
