package optimalalgorithms.leetcode._476.numbercomplement;

public class BitWiseSolver implements NumberComplementSolver {

	public int findComplement(int num) {
		if (num == 0) {
			return 0;
		}
		int negatedNum = num ^ Integer.MAX_VALUE;
		return negatedNum & (Integer.MAX_VALUE >> Integer.numberOfLeadingZeros(num));
	}

}
