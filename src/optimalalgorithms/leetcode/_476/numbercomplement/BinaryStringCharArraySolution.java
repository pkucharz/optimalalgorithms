package optimalalgorithms.leetcode._476.numbercomplement;

public class BinaryStringCharArraySolution implements NumberComplementSolver {

	// 39.32 %
	public int findComplement(int num) {
		if (num == 0) {
			return 0;
		}
		String binaryString = Integer.toBinaryString(num);
		char[] chars = binaryString.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			chars[i] = (chars[i] == '1') ? '0' : '1';
		}
		return Integer.parseInt(new String(chars), 2);
	}

	public static void main(String[] args) {
		System.out.println(new BinaryStringCharArraySolution().findComplement(5));
	}
}
