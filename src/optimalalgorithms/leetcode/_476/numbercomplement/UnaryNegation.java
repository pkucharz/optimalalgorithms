package optimalalgorithms.leetcode._476.numbercomplement;

public class UnaryNegation implements NumberComplementSolver {

	// 26.62%
	@Override
	public int findComplement(int num) {
		int originalBitLength = Integer.toBinaryString(num).length();
		String invertedBinary = Integer.toBinaryString(~num);
		String trimmedHigherBits = invertedBinary.substring(invertedBinary.length() - originalBitLength);
		return Integer.valueOf(trimmedHigherBits, 2);
	}

}
