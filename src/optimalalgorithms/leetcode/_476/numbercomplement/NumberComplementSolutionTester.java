package optimalalgorithms.leetcode._476.numbercomplement;

import java.util.Arrays;
import java.util.Collection;

import optimalalgorithms.SolutionTester;

public class NumberComplementSolutionTester extends SolutionTester<NumberComplementSolver> {

	public static void main(String[] args) {
		new NumberComplementSolutionTester().test();
	}

	@Override
	protected Collection<NumberComplementSolver> solvers() {
		return Arrays.asList(
				new BinaryStringCharArraySolution(),
				new BitWiseSolver(),
				new XorWithHighestOneBitSolver(),
				new UnaryNegation());
	}

	@Override
	protected void randomSolverEvaluation(NumberComplementSolver solver) {
		solver.findComplement(random.nextInt(100));
	}

	@Override
	protected void checkSolver(NumberComplementSolver solver) {
		failIfFalse(solver.findComplement(0) == 0);
		failIfFalse(solver.findComplement(Integer.MAX_VALUE) == 0);
		failIfFalse(solver.findComplement(Integer.MAX_VALUE - 1) == 1);
		failIfFalse(solver.findComplement(1) == 0);
		failIfFalse(solver.findComplement(2) == 1);
		failIfFalse(solver.findComplement(5) == 2);
		failIfFalse(solver.findComplement(8) == 7);
		failIfFalse(solver.findComplement(26) == 5);
	}

}
