package optimalalgorithms.leetcode._476.numbercomplement;

import optimalalgorithms.Solver;

interface NumberComplementSolver extends Solver {

	int findComplement(int num);
}
