package optimalalgorithms.leetcode._476.numbercomplement;

public class XorWithHighestOneBitSolver implements NumberComplementSolver {

	public int findComplement(int num) {
		if (num == 0) {
			return 0;
		}
		int ones = (Integer.highestOneBit(num) << 1) - 1;
		return num ^ ones;
	}

}
